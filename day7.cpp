#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <queue>

enum CardTypes {A = 14, K = 13, Q = 12, J = 11, Ten = 10, Nine = 9, Eight = 8, Seven = 7, Six = 6, Five = 5, Four = 4, Three = 3, Two = 2};

enum HandType {FiveKind = 7, FourKind = 6, FullHouse = 5, ThreeKind = 4, TwoPair = 3, OnePair = 2, HighCard = 1};

class Card
{
	CardTypes Hand[5];

	unsigned int bid;

	HandType hand_type;

	static CardTypes convert_to_card(char InputChar)
	{
		switch (InputChar)
		{
			case 'A':
				return A;
				break;
			case 'K':
				return K;
				break;
			case 'Q': 
				return Q;
				break;
			case 'J':
				return J;
				break;
			case 'T':
				return Ten;
				break;
			case '9':
				return Nine;
				break;
			case '8':
				return Eight;
				break;
			case '7':
				return Seven;
				break;
			case '6':
				return Six;
				break;
			case '5':
				return Five;
				break;
			case '4':
				return Four;
				break;
			case '3':
				return Three;
				break;
			case '2': 
				return Two;
				break;
			default:
				return Two;
		}
	}

	HandType calculate_hand()
	{
		unsigned int pairs_count = 0;
		unsigned int card_count = 0;		//if two found, card_count will be 1;
		unsigned int threekind = 0;
		CardTypes current_card;
		std::queue<unsigned int> found_locations;
		unsigned int count = 0;
		auto Hand = this->Hand;

		for(unsigned int i = 0; i < 5; i++)
		{
			if(i && i == found_locations.front())   //get current card
			{
				//std::cout << "been here: " << i << " already\n";
				found_locations.pop();
				continue;
			}
			current_card = Hand[i];
			//std::cout << "current card: " << current_card << " " << i << "\n";
			for(unsigned int j = i; j < 5; ++j)    //then check the rest
			{
				if (j == i)
					continue;
				if(Hand[j] == current_card)
				{
					found_locations.push(j);
					card_count++;
					//std::cout << "found card" << Hand[j] << "\n";
				}
			}
			if(card_count == 1)
			{
				pairs_count++;
			}
			else if(threekind && card_count == 1)
				return FullHouse;
			else if(card_count == 2)
			{
				threekind = 1;
			}
			else if(card_count == 3)
				return FourKind;
			else if(card_count == 4)
				return FiveKind;
			card_count = 0;
		}
		if(pairs_count == 1)
			return OnePair;
		else if(pairs_count == 2)
			return TwoPair;
		else if(threekind)
			return ThreeKind;
		else return HighCard;
	}

	public:

	unsigned int get_bid()
	{
		return this->bid;
	}


	Card(std::string InputHand)
	{
		unsigned int count = 0;
		std::string buffer = "";
		while(InputHand[count] != ' ')
		{
			this->Hand[count] = this->convert_to_card(InputHand[count]);
			++count;
		}
		count++;
		while (count < InputHand.size())
		{
			buffer.append(1, InputHand[count]);
			count++;
		}
		//std::cout << buffer << "\n";

		unsigned int bid = std::stoi(buffer);
		this->bid = bid;

		//std::cout << "bid loaded\n";
		
		this->hand_type = this->calculate_hand();

		//std::cout << "hand_type: " << hand_type << "\n";
		
	}

	CardTypes * GetHand()
	{
		//returns array, should be sent to auto
		return this->Hand;
	}

	HandType GetHandType()
	{
		return this->hand_type;
	}

	void debugprintall()
	{
		std::cout << "hand_type: " << this->hand_type << "Bid" << this->bid << "\n";
	}
};


std::vector<Card> shitty_sort(std::vector<Card> input)
{
	bool swapped = 1;
	while(swapped)
	{
		swapped = 0;
		for(unsigned int i = 1; i < input.size(); ++i)
		{
			if(input.at(i - 1).GetHandType() > input.at(i).GetHandType())
			{
				swapped = 1;
				Card temp = input.at(i);
				input.at(i) = input.at(i - 1);
				input.at(i - 1) = temp;
			}
			else if(input.at(i - 1).GetHandType() == input.at(i).GetHandType())
			{
				bool i_islarger = 0;
				auto iminusone = input.at(i - 1).GetHand();
				auto ihand = input.at(i).GetHand();
				for(unsigned int j = 0; j < 5; ++j)
				{
					if(iminusone[j] == ihand[j])
						continue;
					else if(iminusone[j] > ihand[j])
						break;
					else
					{
						i_islarger = 1;
						break;
					}
				}
				if(!i_islarger)
				{
					swapped = 1;
					Card temp = input.at(i);
					input.at(i) = input.at(i - 1);
					input.at(i - 1) = temp;
				}
			}
		}
	}
	return input;
}


unsigned int part1()
{
	std::vector<Card> cards;
	std::string line;
	std::ifstream file("day7input.txt");
	unsigned long long total = 0;
	if(file.is_open())
	{
		while(std::getline(file, line))
		{
			Card card(line);
			cards.push_back(card);
		}
	}
	cards.shrink_to_fit();

	cards = shitty_sort(cards);

	for(unsigned int i = 0; i < cards.size(); ++i)
	{
		total += cards.at(i).get_bid() * (i + 1);
		std::cout << cards.at(i).get_bid() << " " << total << "\n";
	}

	//i should probably be passing my own comparator to std::sort, but i cba
	
	return total;
}

int main(int argc, char ** argv)
{
	std::cout << part1() << "\n";
	return 0;
}
