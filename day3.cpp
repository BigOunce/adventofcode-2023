#include <iostream>
#include <fstream>
#include <vector>
#include <string>


std::vector<std::string> get_vector_from_input()
{
	std::vector<std::string> toreturn;
	std::string line;
	std::ifstream file("day3input.txt");

	if(file.is_open())
	{
		while(std::getline(file, line))
		{
			toreturn.push_back(line);
		}
		file.close();
	}
	return toreturn;
}

unsigned int check_above(std::vector<std::string> input, unsigned int i, unsigned int j)
{
	std::string buffer = "";
	if(input.at(i)[j] == '.') //if the square above and to the left is empty
		return 0;
	while(j != 0 && input.at(i)[j -1] != '.') // go to the start of the number
	{
		j--;
	}
	while(j < input.at(i).size() && input.at(i)[j] != '.')
	{
		buffer.append(1, input.at(i)[j]);
		++j;
	}
	return std::stoi(buffer);
}

unsigned int check_left(std::vector<std::string> input, unsigned int i, unsigned int j)
{
	std::string buffer = "";
	if(input.at(i)[j] == '.')
		return 0;
	while(j != 0 && input.at(i)[j - 1] != '.')
		--j;
	while((input.at(i)[j] >= 48 && input.at(i)[j] <= 57))
	{
		buffer.append(1, input.at(i)[j]);
		++j;
	}
	return std::stoi(buffer);
}

unsigned int check_right(std::vector<std::string> input, unsigned int i, unsigned int j)
{
	std::string buffer = "";
	if(input.at(i)[j] == '.')
		return 0;
	while(j < input.at(i).size() && input.at(i)[j] != '.')
	{
		buffer.append(1, input.at(i)[j]);
		++j;
	}
	return std::stoi(buffer);
}

unsigned int sum_valid_gears(std::vector<std::string> input, unsigned int i, unsigned int j)
{
	std::vector<unsigned int> sums;
	unsigned int current;
	current = check_above(input, i - 1, j); //check up
	if(current)
		sums.push_back(current);
	else					//otherwise check up-diags
	{
		current = check_above(input, i - 1, j - 1);
		if(current)
			sums.push_back(current);
		current = check_above(input, i - 1, j + 1);
		if(current)
			sums.push_back(current);
	}
	current = check_left(input, i , j - 1); //left
	if(current)
		sums.push_back(current);
	current = check_right(input, i, j + 1); //right
	if(current)
		sums.push_back(current);	
	current = check_above(input, i + 1, j); //down
	if(current)
		sums.push_back(current);
	else					//otherwise check down-diags
	{
		current = check_above(input, i + 1, j -1);
		if(current)
			sums.push_back(current);
		current = check_above(input, i + 1, j + 1);
		if(current)
			sums.push_back(current);
	}

	if(sums.size() != 2)
		return 0;
	return sums.at(0) * sums.at(1);



}
unsigned int sum_number_neighbours(std::vector<std::string> input, unsigned int i, unsigned int j) // i'll check cardinals first, then diagonals.
{
	unsigned int above = 0; //could be done neater with only two ints but fuck it
	unsigned int up_left = 0;
	unsigned int up_right = 0;
	unsigned int left = 0;
	unsigned int right = 0;
	unsigned int down_right = 0; 
	unsigned int down = 0;
	unsigned int down_left = 0;
	above = check_above(input, i - 1 , j);
	if (!above)
	{
		up_left = check_above(input, i - 1, j - 1);
		up_right = check_above(input, i - 1, j + 1);
	}
	left = check_left(input, i , j - 1);
	right = check_right(input, i, j + 1);
	down = check_above(input, i + 1, j);
	if (!down)
	{
		down_left = check_above(input, i + 1, j -1);
		down_right = check_above(input, i + 1, j + 1);
	}

	//std::cout << above << " " << up_left << " " << up_right << " " << left << " " << right << " " << down_right << " " << down << " " << down_left << "\n";

	return above + up_left + up_right + left + right + down_right + down + down_left;
}

unsigned int part2()
{
	unsigned int sum = 0;
	std::vector<std::string> input = get_vector_from_input();
	for(unsigned int i = 0; i < input.size(); ++i)
	{
		for(unsigned int j = 0; j < input.size(); ++j)
		{
			if((input.at(i)[j] != '.') && !(input.at(i)[j] >= 48 && input.at(i)[j] <= 57)) // if a symbol
				sum += sum_valid_gears(input, i , j);
		}
	}
	return sum;
}


unsigned int part1()
{
	unsigned int sum = 0;
	std::vector<std::string> input = get_vector_from_input();
	for(unsigned int i = 0; i < input.size(); ++i)
	{
		for(unsigned int j = 0; j < input.size(); ++j)
		{
			if((input.at(i)[j] != '.') && !(input.at(i)[j] >= 48 && input.at(i)[j] <= 57)) // if a symbol
				sum += sum_number_neighbours(input, i, j);
		}
	}
	return sum;
}

int main(int argc, char** argv)
{
	std::cout << part1() << "\n";
	std::cout << part2() << "\n";
	return 0;
}
