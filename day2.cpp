#include <iostream>
#include <fstream>
#include <string>
#include <regex>
#include <vector>

const std::regex FIND_RED("[0-9]+(?=( red))");

const std::regex FIND_GREEN("[0-9]+(?=( green))");

const std::regex FIND_BLUE("[0-9]+(?=( blue))");

const std::regex GAME_NUMBER(R"([0-9]+(?=:))");

int get_game_number(std::string line)
{
	std::smatch number; //where the matches go, as strings
	if(std::regex_search(line, number, GAME_NUMBER))
	{
		std::string strumber = number[0];
		return std::stoi(strumber);
	}
	std::cerr << "failed\n";
	return -1;
}

unsigned int find_lowest(std::string line, std::regex colourfinder)
{
	std::sregex_iterator iter(line.begin(), line.end(), colourfinder);
	std::sregex_iterator end;
	
	std::vector<unsigned int> counts;

	unsigned int maximum = 0;

	while(iter != end) //get all the instances of the regex on the line. normally it just stops on the first match
	{
		std::string number = (*iter)[0];
		if(number != "")
			counts.push_back(std::stoi(number));
		++iter;
	}

	for(unsigned int i = 0; i < counts.size(); ++i)
		if(counts.at(i) > maximum)
			maximum = counts.at(i);

	return maximum;
}

int validate_colour(std::string line, std::regex colourfinder, unsigned int maximum)
{
	std::sregex_iterator iter(line.begin(), line.end(), colourfinder);
	std::sregex_iterator end;

	std::vector<unsigned int> counts;

	while(iter != end)
	{

		std::string number = (*iter)[0];
		if(number != "")
			counts.push_back(std::stoi(number)); //pushes back the first capturing group, which is the numbers.
							 //after converting it to an integer, first.

		++iter;
	}
	for(unsigned int i = 0; i < counts.size(); ++i)
	{
		if(counts.at(i) > maximum)
			return 0;
	}
	return 1;
}

unsigned int part2()
{
	std::string line;
	std::ifstream file("day2input.txt");

	unsigned long sumofpowersets = 0;

	if(file.is_open())
	{
		while(std::getline(file, line))
		{
			unsigned int minimumred = find_lowest(line, FIND_RED);
			unsigned int minimumgreen = find_lowest(line, FIND_BLUE);
			unsigned int minimumblue =  find_lowest(line, FIND_GREEN);
			sumofpowersets += (minimumblue * minimumgreen * minimumred);
		}
		file.close();
	}
	return sumofpowersets;
}

unsigned int part1()
{
	std::string line;
	std::ifstream file("day2input.txt");

	unsigned int totalvalidgamenumbers = 0;

	const unsigned int MAX_RED = 12;
	const unsigned int MAX_GREEN = 13;
	const unsigned int MAX_BLUE = 14;

	if(file.is_open())
	{
		while(std::getline(file, line))
		{
			if(validate_colour(line, FIND_RED, MAX_RED) && validate_colour(line, FIND_BLUE, MAX_BLUE) && validate_colour(line, FIND_GREEN, MAX_GREEN))
				totalvalidgamenumbers += get_game_number(line);
		}
		file.close();
	}
	return totalvalidgamenumbers;
}

int main(int args, char ** argv)
{
	std::cout << part1() << "\n";
	std::cout << part2() << "\n";
	return 0;
}
