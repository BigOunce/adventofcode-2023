#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <tuple>


std::vector<std::string> * get_text_input()
{
	std::vector<std::string> * textinput = new std::vector<std::string>();
	std::fstream file("day6input.txt");
	std::string line;
	if(file.is_open())
	{
		while(std::getline(file, line))
		{
			textinput->push_back(line);
		}
		file.close();
	}
	return textinput;
}

std::vector<unsigned long long> get_numbers_on_line2(std::string line)
{
	std::vector<unsigned long long> toreturn;
	std::string buffer = "";
	unsigned int count = 0;
	while(count < line.size())
	{
		if(line[count] >= '0' && line[count] <= '9')
		{
			buffer.append(1, line[count]);
		}
		count++;
	}
	toreturn.push_back(std::stoull(buffer));
	return toreturn;
}

std::vector<unsigned long long> get_numbers_on_line(std::string line)
{
	std::vector<unsigned long long> toreturn;
	std::string buffer = "";
	unsigned int count = 0;
	while(count < line.size())
	{
		if(line[count] >= '0' && line[count] <= '9')
		{
			buffer.append(1, line[count]);
			if(line[count + 1] == ' ')
				{
					toreturn.push_back(std::stoull(buffer));
					buffer = "";
				}
		}
		count++;
	}
	toreturn.push_back(std::stoull(buffer));
	return toreturn;
}

std::vector<std::pair<unsigned long long, unsigned long long>> get_times_and_distances2(std::vector<std::string> * textinput)
{
	std::vector<unsigned long long> times = get_numbers_on_line2(textinput->at(0));
	std::vector<unsigned long long> distances = get_numbers_on_line2(textinput->at(1));
	std::vector<std::pair<unsigned long long, unsigned long long>> toreturn;
	for(unsigned int i = 0; i < times.size(); i++)
	{
		std::pair<unsigned long long, unsigned long long> topush(times.at(i), distances.at(i));
		toreturn.push_back(topush);
	}
	return toreturn;
}


std::vector<std::pair<unsigned long long, unsigned long long>> get_times_and_distances(std::vector<std::string> * textinput)
{
	std::vector<unsigned long long> times = get_numbers_on_line(textinput->at(0));
	std::vector<unsigned long long> distances = get_numbers_on_line(textinput->at(1));
	std::vector<std::pair<unsigned long long, unsigned long long>> toreturn;
	for(unsigned int i = 0; i < times.size(); i++)
	{
		std::pair<unsigned long long, unsigned long long> topush(times.at(i), distances.at(i));
		toreturn.push_back(topush);
	}
	return toreturn;
}

std::vector<unsigned long long> winning_combos(std::pair<unsigned long long, unsigned long long> times_and_distance)
{
	std::vector<unsigned long long> toreturn;
	unsigned long long time = std::get<0>(times_and_distance);
	unsigned long long distance = std::get<1>(times_and_distance);
	for(unsigned int i = 0; i < time; i++)
	{
		unsigned long long distance_travelled = i * (time - i);
		if(distance_travelled > distance)
			toreturn.push_back(distance_travelled);
	}
	return toreturn;
}

unsigned long long part2()
{
	std::vector<std::string> * textinput = get_text_input();
	std::vector<std::pair<unsigned long long, unsigned long long>> times_and_distances = get_times_and_distances2(textinput);
	delete textinput;
	std::cout << "times and distances got\n";
	unsigned long long allnumbers = 1;
	std::vector<std::vector<unsigned long long>> all_winning_combos;
	for(unsigned int i = 0; i < times_and_distances.size(); ++i)
	{
		all_winning_combos.push_back(winning_combos(times_and_distances.at(i)));
	}
	std::cout << "combos pushed\n";

	for(unsigned int i = 0; i < all_winning_combos.size(); ++i)
	{
		allnumbers = allnumbers * all_winning_combos.at(i).size();
	}
	std::cout << "bruh\n";


	return allnumbers;
}

unsigned long long part1()
{
	std::vector<std::string> * textinput = get_text_input();
	std::vector<std::pair<unsigned long long, unsigned long long>> times_and_distances = get_times_and_distances(textinput);
	delete textinput;
	std::cout << "times and distances got\n";
	unsigned long long allnumbers = 1;
	std::vector<std::vector<unsigned long long>> all_winning_combos;
	for(unsigned int i = 0; i < times_and_distances.size(); ++i)
	{
		all_winning_combos.push_back(winning_combos(times_and_distances.at(i)));
	}
	std::cout << "combos pushed\n";

	for(unsigned int i = 0; i < all_winning_combos.size(); ++i)
	{
		allnumbers = allnumbers * all_winning_combos.at(i).size();
	}
	std::cout << "bruh\n";


	return allnumbers;
}


int main()
{
	std::cout << part1() << "\n";
	std::cout << part2() << "\n";
	return 0;
}
