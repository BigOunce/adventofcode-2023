#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <tuple>
#include <algorithm>


std::tuple<std::vector<unsigned int>, std::vector<unsigned int>> get_numbers(std::string line)
{
	std::vector<unsigned int> winning_numbers;
	std::vector<unsigned int> has_numbers;
	unsigned int count = 0;
	std::string currentstrumber = "";
	while(line[count] != ':') 	// go to the winning numbers
		count++;
	
	while(line[count] != '|') 	// while not at the delimeter
	{
		if(line[count] >= '0' && line[count] <= '9') //if a number
		{
			currentstrumber.append(1, line[count]);
			if(line[count + 1] == ' ')
			{
				winning_numbers.push_back(std::stoi(currentstrumber));
				currentstrumber = "";
			}
		}
		count++;
	}
	while(count < line.size())
	{
		if(line[count] >= '0' && line[count] <= '9') //if a number
		{
			currentstrumber.append(1, line[count]);
			if(line[count + 1] == ' ')
			{
				has_numbers.push_back(std::stoi(currentstrumber));
				currentstrumber = "";
			}
		}
		++count;
	}
	has_numbers.push_back(std::stoi(currentstrumber));

	return std::make_tuple(winning_numbers, has_numbers);
}

std::vector<unsigned int> get_matching_numbers(std::tuple<std::vector<unsigned int>, std::vector<unsigned int>> numbers)
{
	std::vector<unsigned int> intersection;
	std::vector<unsigned int> * winning_numbers = &std::get<0>(numbers);
	std::vector<unsigned int> * has_numbers = &std::get<1>(numbers);
	std::sort(winning_numbers->begin(), winning_numbers->end());
	std::sort(has_numbers->begin(), has_numbers->end());
	std::set_intersection(winning_numbers->begin(), winning_numbers->end(), has_numbers->begin(), has_numbers->end(), std::back_inserter(intersection));
	return intersection;
}

unsigned int line_winning_numbers(std::tuple<std::vector<unsigned int>, std::vector<unsigned int>> numbers)
{
	std::vector<unsigned int> intersection = get_matching_numbers(numbers);
	return intersection.size();
}

unsigned int work_out_points(std::tuple<std::vector<unsigned int>, std::vector<unsigned int>> numbers)
{
	std::vector<unsigned int> intersection = get_matching_numbers(numbers);
	if(!intersection.size()) // if size = 0
		return 0;
	//std::cout << "calculated: " << (1 << (intersection.size() - 1)) << "\n";
	return 1 << (intersection.size() - 1);
}

unsigned int part2()
{
	unsigned int cardtotal = 0;
	std::string line;
	std::fstream file("day4input.txt");
	std::vector<std::tuple<unsigned int, std::vector<unsigned int>, std::vector<unsigned int>>> cardsncount;

	if(file.is_open())
	{
		while(std::getline(file,line))
		{
			std::tuple<std::vector<unsigned int>, std::vector<unsigned int>> numbers = get_numbers(line); //get numbers
			std::vector<unsigned int> * winning_numbers = &std::get<0>(numbers);
			std::vector<unsigned int> * has_numbers = &std::get<1>(numbers);
			cardsncount.push_back(std::make_tuple(1, *winning_numbers, *has_numbers)); //and for each one get the counts
		}
	}
	for(unsigned int i = 0; i < cardsncount.size(); ++i)
	{
		unsigned int cardcount = std::get<0>(cardsncount.at(i));
		std::vector<unsigned int> * winning_numbers = &std::get<1>(cardsncount.at(i)); //terrible, should probably have assigned to heap
		std::vector<unsigned int> * has_numbers = &std::get<2>(cardsncount.at(i));
		std::tuple<std::vector<unsigned int>, std::vector<unsigned int>> numbers = make_tuple(*winning_numbers, *has_numbers);
		unsigned int winning_count = line_winning_numbers(numbers);
		for(unsigned int j = 0; j < cardcount; ++j)
			for(unsigned int k = 1; k < winning_count + 1; ++k)
			{
				unsigned int accessor = i + k;
				std::get<0>(cardsncount.at(accessor))++;
			}

	}

	for(unsigned int i = 0; i < cardsncount.size(); ++i)
		cardtotal+= std::get<0>(cardsncount.at(i));
	return cardtotal;
}

unsigned int part1()
{
	unsigned int pointtotal = 0;
	std::string line;
	std::fstream file("day4input.txt");
	if(file.is_open())
	{
		while(std::getline(file, line))
		{
			std::tuple<std::vector<unsigned int>, std::vector<unsigned int>> numbers = get_numbers(line);
			pointtotal += work_out_points(numbers);
		}
	}

	return pointtotal;
}

int main()
{
	std::cout << part1() << "\n";
	std::cout << part2() << "\n";
	return 0;
}
