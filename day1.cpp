#include <iostream>
#include <fstream>
#include <string>
#include <regex>

const std::regex wordfind("one|two|three|four|five|six|seven|eight|nine");

char findnumberword(std::string input)
{
	std::smatch match;
	if(std::regex_search(input, match, wordfind))
	{
		std::string found = match[0];

		if(found == "one")
			return '1';
		else if(found == "two")
			return '2';
		else if(found == "three")
			return '3';
		else if(found == "four")
			return '4';
		else if(found == "five")
			return '5';
		else if(found == "six")
			return '6';
		else if(found == "seven")
			return '7';
		else if(found == "eight")
			return '8';
		else if(found == "nine")
			return '9';
	}
	return 0;
}

unsigned int part2()
{
	//ok so i want to find out the first word and then when i see a second word or number replace the second with that
	std::string line;
	std::ifstream file("day1input.txt");
	unsigned int total = 0;
	if(file.is_open())
	{
		while(std::getline(file, line))
		{
			unsigned int count = 0;
			char firstint = 0;
			char secondint = 0;
			char wordnumber = 0;
			std::string combined = "";
			std::string functioninput = "";
			while (count < line.size())
			{
				char current = line[count];
				switch (current)
				{	
					case '1':
						wordnumber = findnumberword(functioninput);
						if(wordnumber)
						{
							if(!firstint)
								firstint = wordnumber;
							else
								secondint = wordnumber;
							functioninput = "";
						}
						if(!firstint)
							firstint = current;
						else
							secondint = current;
						functioninput = "";
						wordnumber = 0;
						break;
					case '2':
						wordnumber = findnumberword(functioninput);
						if(wordnumber)
						{
							if(!firstint)
								firstint = wordnumber;
							else
								secondint = wordnumber;
							functioninput = "";
						}
						if(!firstint)
							firstint = current;
						else
							secondint = current;
						functioninput = "";
						wordnumber = 0;
						break;

					case '3':
						wordnumber = findnumberword(functioninput);
						if(wordnumber)
						{
							if(!firstint)
								firstint = wordnumber;
							else
								secondint = wordnumber;
							functioninput = "";
						}
						if(!firstint)
							firstint = current;
						else
							secondint = current;
						functioninput = "";
						wordnumber = 0;
						break;

					case '4':
						wordnumber = findnumberword(functioninput);
						if(wordnumber)
						{
							if(!firstint)
								firstint = wordnumber;
							else
								secondint = wordnumber;
							functioninput = "";
						}
						if(!firstint)
							firstint = current;
						else
							secondint = current;
						functioninput = "";
						wordnumber = 0;
						break;
					case '5':
						wordnumber = findnumberword(functioninput);
						if(wordnumber)
						{
							if(!firstint)
								firstint = wordnumber;
							else
								secondint = wordnumber;
							functioninput = "";
						}
						if(!firstint)
							firstint = current;
						else
							secondint = current;
						functioninput = "";
						wordnumber = 0;
						break;
					case '6':
						wordnumber = findnumberword(functioninput);
						if(wordnumber)
						{
							if(!firstint)
								firstint = wordnumber;
							else
								secondint = wordnumber;
							functioninput = "";
						}
						if(!firstint)
							firstint = current;
						else
							secondint = current;
						functioninput = "";
						wordnumber = 0;
						break;
					case '7':
						wordnumber = findnumberword(functioninput);
						if(wordnumber)
						{
							if(!firstint)
								firstint = wordnumber;
							else
								secondint = wordnumber;
							functioninput = "";
						}
						if(!firstint)
							firstint = current;
						else
							secondint = current;
						functioninput = "";
						wordnumber = 0;
						break;
					case '8':
						wordnumber = findnumberword(functioninput);
						if(wordnumber)
						{
							if(!firstint)
								firstint = wordnumber;
							else
								secondint = wordnumber;
							functioninput = "";
						}
						if(!firstint)
							firstint = current;
						else
							secondint = current;
						functioninput = "";
						wordnumber = 0;
						break;

					case '9':
						wordnumber = findnumberword(functioninput);
						if(wordnumber)
						{
							if(!firstint)
								firstint = wordnumber;
							else
								secondint = wordnumber;
							functioninput = "";
						}
						if(!firstint)
							firstint = current;
						else
							secondint = current;
						functioninput = "";
						wordnumber = 0;
						break;
					default:
						functioninput.append(1, current);
						wordnumber = findnumberword(functioninput);
						if(wordnumber)
						{
							if(!firstint)
								firstint = wordnumber;
							else
								secondint = wordnumber;
							functioninput = current;
						}
						wordnumber = 0;

						break;
				}
				count++;
			}
			wordnumber = findnumberword(functioninput);
			if(wordnumber)
			{
				if(!firstint)
					firstint = wordnumber;
				else
					secondint = wordnumber;
			}

			if(!secondint)
			{
				combined.append(1, firstint);
				combined.append(1, firstint);
			}
			else
			{
				combined.append(1, firstint);
				combined.append(1, secondint);
			}
			total += std::stoi(combined);			
		}
		file.close();
	}
	return total;
}

unsigned int part1()
{
	std::string line;
	std::ifstream file("day1input.txt");
	unsigned int total = 0;
	if(file.is_open())
	{
		while(std::getline(file, line))
		{
			unsigned int count = 0;
			char first = 0;
			char second = 0;
			std::string combined = "";
			while (count < line.size())
			{
				char current = line[count];
				switch (current)
				{	
					case '1':
						if(!first)
							first = current;
						else
							second = current;
						break;
					case '2':
						if(!first)
							first = current;
						else
							second = current;
						break;
					case '3':
						if(!first)
							first = current;
						else
							second = current;
						break;
					case '4':
						if(!first)
							first = current;
						else
							second = current;
						break;
					case '5':
						if(!first)
							first = current;
						else
							second = current;
						break;
					case '6':
						if(!first)
							first = current;
						else
							second = current;
						break;
					case '7':
						if(!first)
							first = current;
						else
							second = current;
						break;
					case '8':
						if(!first)
							first = current;
						else
							second = current;
						break;
					case '9':
						if(!first)
							first = current;
						else
							second = current;
						break;
					default:
						break;
				}
				count++;
			}
			if(!second)
			{
				combined.append(1, first);
				combined.append(1, first);
			}
			else
			{
				combined.append(1, first);
				combined.append(1, second);
			}
			total += std::stoi(combined);			
		}
		file.close();
	}
	return total;
}

int main(int argc, char ** argv)
{
	std::cout << part1() << "\n";
	std::cout << part2() << "\n";
	return 0;
}
