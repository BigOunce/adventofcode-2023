#include <iostream>
#include <vector>
#include <fstream>
#include <string>

std::vector<unsigned long long> get_numbers_on_line(std::string line)
{
	std::vector<unsigned long long> toreturn;
	std::string buffer = "";
	unsigned int count = 0;
	while(count < line.size())
	{
		if(line[count] >= '0' && line[count] <= '9')
		{
			buffer.append(1, line[count]);
			if(line[count + 1] == ' ')
				{
					toreturn.push_back(std::stoull(buffer));
					buffer = "";
				}
		}
		count++;
	}
	toreturn.push_back(std::stoull(buffer));
	return toreturn;
}

std::vector<std::string> load_text()
{
	std::vector<std::string> toreturn;
	std::string line;
	std::ifstream file("day5input.txt");
	if(file.is_open())
	{
		while(std::getline(file, line))
			toreturn.push_back(line);
		file.close();
	}
	return toreturn;
}

std::vector<std::vector<unsigned long long>> get_category(std::vector<std::string> textinput, std::string search)
{
	std::vector<std::vector<unsigned long long>> seed_to_soil;
	unsigned int linenumber = 0;
	while(textinput.at(linenumber) != search)
	{
		++linenumber;
	}
	++linenumber;
	while(linenumber < textinput.size() && (textinput.at(linenumber)[0] >= '0' && textinput.at(linenumber)[0] <= '9')) // this sucks
	{
		seed_to_soil.push_back(get_numbers_on_line(textinput.at(linenumber)));
		++linenumber;
	}

	return seed_to_soil;
}

unsigned long long work_out_transition(std::vector<std::vector<unsigned long long>> * transitions, unsigned long long currentvalue)
{
	unsigned int range_length;
	unsigned int destination_range_start;
	unsigned int source_range_start;
	unsigned int range_end_difference;
	bool flag = 0; //awful
	for(unsigned int i = 0; i < transitions->size(); ++i)
	{
		range_length = transitions->at(i).at(2);
		source_range_start = transitions->at(i).at(1);
		destination_range_start = transitions->at(i).at(0);
		if(currentvalue >= source_range_start && currentvalue <= (source_range_start + (range_length - 1)))
		{
			flag = 1;
			break;
		}
	}
	if(!flag)
		return currentvalue;
	range_end_difference = (source_range_start + (range_length - 1)) - currentvalue;
	unsigned int category_number = (destination_range_start + (range_length - 1)) - range_end_difference;
	return category_number;
}

unsigned long long work_out_location(std::vector<std::vector<std::vector<unsigned long long>> * > combined_maffs, unsigned long long seed)
{
	unsigned long long current = seed;
	for(unsigned int i = 0; i < combined_maffs.size(); ++i) // each transition
	{
		current = work_out_transition(combined_maffs.at(i), current); // each category of transition
	}
	return current;

}

std::vector<unsigned long long> get_location_numbers(std::vector<std::vector<std::vector<unsigned long long>> * > combined_maffs, std::vector<unsigned long long> seeds)
{
	std::vector<unsigned long long> location_numbers;
	for(unsigned int i = 0; i < seeds.size(); ++i)
		location_numbers.push_back(work_out_location(combined_maffs, seeds.at(i)));
	return location_numbers;
}


std::vector<unsigned long long> get_location_numbers_2(std::vector<std::vector<std::vector<unsigned long long>> * > combined_maffs, std::vector<unsigned long long> seeds)
{
	std::vector<unsigned long long> location_numbers;
	unsigned int seeds_size = seeds.size();
	for(unsigned int i = 0; i < seeds.size(); ++i)
	{ 
		std::cout << "on seed: " << i << " of " << seeds_size << "\n";
		location_numbers.push_back(work_out_location(combined_maffs, seeds.at(i)));
	}
	return location_numbers;
}

unsigned int min_element(std::vector<unsigned long long> chungus) //why isn't this in the standard library
{
	unsigned long long wow = -1;
	for(unsigned int i = 0; i < chungus.size(); ++i)
		if(chungus.at(i) < wow)
			wow = chungus.at(i);
	return wow;
}


unsigned long long part1()
{
	std::vector<std::string> textinput = load_text();
	std::vector<unsigned long long> seeds = get_numbers_on_line(textinput.at(0));
	std::vector<std::vector<unsigned long long>> seed_to_soil = get_category(textinput, "seed-to-soil map:"); //could do this more efficiently with a shared count variable to make it 
														  //properly O(n) but i cba
	std::vector<std::vector<unsigned long long>> soil_to_fertiliser = get_category(textinput, "soil-to-fertilizer map:");
	std::vector<std::vector<unsigned long long>> fertiliser_to_water = get_category(textinput, "fertilizer-to-water map:"); //as it stands it resets the counter and scans the whole file
	std::vector<std::vector<unsigned long long>> water_to_light = get_category(textinput, "water-to-light map:");		// for each category
	std::vector<std::vector<unsigned long long>> light_to_temperature = get_category(textinput, "light-to-temperature map:");
	std::vector<std::vector<unsigned long long>> temperature_to_humidity = get_category(textinput, "temperature-to-humidity map:");
	std::vector<std::vector<unsigned long long>> humidity_to_location = get_category(textinput, "humidity-to-location map:");
	// data sorted, now to faff.
	
	std::vector<std::vector<std::vector<unsigned long long>> * > combined_maffs; //pointers to the sums
	combined_maffs.push_back(&seed_to_soil);
	combined_maffs.push_back(&soil_to_fertiliser);
	combined_maffs.push_back(&fertiliser_to_water);
	combined_maffs.push_back(&water_to_light);
	combined_maffs.push_back(&light_to_temperature);
	combined_maffs.push_back(&temperature_to_humidity);
	combined_maffs.push_back(&humidity_to_location);

	std::vector<unsigned long long> location_numbers = get_location_numbers(combined_maffs, seeds);

	return min_element(location_numbers); //why 
}


std::vector<unsigned long long> expand_seed_range(std::vector<unsigned long long> seeds)
{
	std::vector<unsigned long long> expanded_seeds;
	for(unsigned int i = 0; i < seeds.size(); i = i+2)
	{
		unsigned long long start_range = seeds.at(i);
		unsigned long long range_length = seeds.at(i+1);
		unsigned long long end_range = start_range + range_length;
		unsigned long long todo = end_range - start_range;
		std::cout << " expanding i: " <<i << " " << todo << " left\n";
		for(unsigned long long j = start_range; j < end_range; j++)
			expanded_seeds.push_back(j);
	}
	return expanded_seeds;
}

unsigned long long part2()
{
	std::vector<std::string> textinput = load_text();
	std::vector<unsigned long long> seeds = get_numbers_on_line(textinput.at(0));
	std::vector<unsigned long long> expanded_seeds = expand_seed_range(seeds);
	std::cout << "seeds expanded\n";
	std::vector<std::vector<unsigned long long>> seed_to_soil = get_category(textinput, "seed-to-soil map:"); //could do this more efficiently with a shared count variable to make it 
														  //properly O(n) but i cba
	std::vector<std::vector<unsigned long long>> soil_to_fertiliser = get_category(textinput, "soil-to-fertilizer map:");
	std::vector<std::vector<unsigned long long>> fertiliser_to_water = get_category(textinput, "fertilizer-to-water map:"); //as it stands it resets the counter and scans the whole file
	std::vector<std::vector<unsigned long long>> water_to_light = get_category(textinput, "water-to-light map:");		// for each category
	std::vector<std::vector<unsigned long long>> light_to_temperature = get_category(textinput, "light-to-temperature map:");
	std::vector<std::vector<unsigned long long>> temperature_to_humidity = get_category(textinput, "temperature-to-humidity map:");
	std::vector<std::vector<unsigned long long>> humidity_to_location = get_category(textinput, "humidity-to-location map:");
	// data sorted, now to faff.
	
	std::vector<std::vector<std::vector<unsigned long long>> * > combined_maffs; //pointers to the sums
	combined_maffs.push_back(&seed_to_soil);
	combined_maffs.push_back(&soil_to_fertiliser);
	combined_maffs.push_back(&fertiliser_to_water);
	combined_maffs.push_back(&water_to_light);
	combined_maffs.push_back(&light_to_temperature);
	combined_maffs.push_back(&temperature_to_humidity);
	combined_maffs.push_back(&humidity_to_location);

	std::vector<unsigned long long> location_numbers = get_location_numbers_2(combined_maffs, expanded_seeds);

	return min_element(location_numbers); //why 
}


int main()
{
	std::cout << part1() << "\n";
	std::cout << part2() << "\n";
	return 0;
}
